/*
 * Public API Surface of mat-prepared-table
 */

export * from './lib/model/table-inputs';
export * from './lib/mat-prepared-table.service';
export * from './lib/component/mat-prepared-table/mat-prepared-table.component';
export * from './lib/mat-prepared-table.module';
