import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'lib-search-text-box',
  templateUrl: './search-text-box.component.html',
  styleUrls: ['./search-text-box.component.scss']
})
export class SearchTextBoxComponent implements OnInit {
  @Input() labelText: string;

  @Output() searchClicked = new EventEmitter<string>();

  searchTextBoxValue: string;

  constructor() { }

  ngOnInit() {
  }

  search() {
    this.searchClicked.emit(this.searchTextBoxValue);
  }

  clear() {
    this.searchTextBoxValue = '';
    this.search();
  }
}
