import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ITableInputs } from '../../model/table-inputs';
import { IDataGridEntity } from '../../model/data-grid-entity';
import { PageEvent } from '@angular/material/paginator';
import { ControlType } from '../../model/enums';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'mat-prepared-table',
  templateUrl: './mat-prepared-table.component.html',
  styleUrls: ['./mat-prepared-table.component.scss']
})
export class MatPreparedTableComponent implements OnInit {
  /** Table configs to design UI of table & displaying data */
  @Input() tableInputs: ITableInputs<any>;

  /** Enable this property if you wanna show table in small screen or inside of a dialog */
  @Input() isGridInDialog = false;

  /** If set to true then remove whole div container of paginator & pagination */
  @Input() removeContainerOfPagingBlock = false;

  /** Click event emitter of back btn */
  @Output() backClicked: EventEmitter<any> = new EventEmitter();

  /** Number of data displaying in each page of table */
  public pageSize = 5;

  /** Model of req from API */
  public dataGridEntity: IDataGridEntity;

  /** Number of page of tbl */
  public pageNum = 0;

  /** Total pages for data loaded in tbl */
  public totalPages = 0;

  /** Col field names for sorting */
  public sortColNames: string[] = [];

  /** Field names for no sorting */
  public noSortCols: string[] = [];

  /** ControlType enum var for using HTML side */
  public controlType = ControlType;

  /**
   * Cols for display data
   */
  public displayedColumns: string[];

  constructor() { }

  ngOnInit() {
  }

  /** Back btn event */
  public back(): void {
    this.backClicked.emit();
  }

  /**
   * Event of click of search btn
   * This method emits loadData EventEmitter automatically
   * @param searchValue Value entered in search text box
   */
  public onClickSearch(searchValue: string) {
    this.dataGridEntity.searchText = searchValue;
    this.loadData.emit(this.dataGridEntity);
  }

  /**
   * Event of change of paginator
   * This method emits loadData EventEmitter automatically
   * @param paginator Paginator page change event param
   */
  public gridPageChange(paginator: PageEvent) {
    this.dataGridEntity.pageIdx = paginator.pageIndex;
    this.dataGridEntity.pageSize = paginator.pageSize;
    this.loadData.emit(this.dataGridEntity);
  }

  /**
   * Event of enter page number manually
   * This method emits loadData EventEmitter automatically
   * @param page Keyboard event
   */
  public onPageEnter(page: KeyboardEvent): void {
    let pageEntered: number = Number((page.target as HTMLInputElement).value);
    this.pageNum = pageEntered;
    if (pageEntered > this.totalPages) { pageEntered = this.totalPages; }
    this.dataGridEntity.pageIdx = pageEntered - 1;
    this.loadData.emit(this.dataGridEntity);
  }

  /**
   * Table checked change event
   * This method emits gridCheckedChanged EventEmitter automatically
   * @param chk Checkbox component
   * @param item Item selected in table
   */
  public onGridCheckedChanged(chk: MatCheckboxChange, item: any) {
    const send: IGridCellModify = {
      id: chk.source.id,
      name: chk.source.name,
      item,
      checked: chk.checked
    };
    this.gridCheckedChanged.emit(send);
  }

  /**
   * Event of change of text of cell
   * This method emits gridTextChanged EventEmitter automatically
   * @param text Text entered in cell
   * @param item Selected item in tbl
   */
  public onGridCellTextChanged(text: any, item: any) {
    const send: IGridCellModify = {
      id: text.srcElement.id,
      name: text.srcElement.name,
      item,
      text: text.srcElement.value
    };
    this.gridTextChanged.emit(send);
  }

  /**
   * Prevent enter char in cell not match to item regex (item.regex)
   * @param e Char entered in cell
   * @param item Selected item
   */
  public onGridCellTextKeypress(e: any, item: any): void {
    if (item.regex && !this.getRegex(e.key, item.regex)) { e.preventDefault(); }
  }

  /**
   * If ControlType of cell is Link then emits linkClicked EvnetEmitter
   * @param item Selected item
   */
  public onLinkClick(item: any): void {
    this.linkClicked.emit(item);
  }

  /**
   * Event for dbl click on an item
   * This method set selectedItem & selectemItems & emits rowDblClicked EventEmitter automatically
   * @param row Dbl clicked item
   */
  public onDoubleClick(row: any): void {
    this.tableInputs.data.selectedItem = row;

    if (this.tableInputs.multiSelector) {
      this.selectedItems.push(row);
      this.rowDblClicked.emit(this.selectedItems);
      return;
    }

    this.rowDblClicked.emit(row);
  }
}
