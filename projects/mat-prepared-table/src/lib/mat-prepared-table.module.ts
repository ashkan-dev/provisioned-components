import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatPreparedTableComponent } from './component/mat-prepared-table/mat-prepared-table.component';
import { CommonModule } from '@angular/common';
import { SearchTextBoxComponent } from './component/search-text-box/search-text-box.component';
import { MatPreparedTableService } from './mat-prepared-table.service';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
  declarations: [
    MatPreparedTableComponent,
    SearchTextBoxComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule
  ],
  exports: [MatPreparedTableComponent],
  providers: [MatPreparedTableService]
})
export class MatPreparedTableModule { }
