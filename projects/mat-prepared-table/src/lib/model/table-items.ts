import { KeyValue } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { IFilterComplexItems } from './filter-complex-items';
import { ControlType } from './enums';

/** Provisioning table data related model */
export interface ITableItems<T> {
    /**
     * Prepare table column header label & field name.
     * key is header label & value is field name.
     */
    columns: KeyValue<string, string>[];

    /**
     * Specify your cell of table component type.
     * If you just wanna show the data then don't need to fill this prop.
     */
    cellControls?: ControlType[];

    /** Items to display in table */
    items: MatTableDataSource<T>;

    /** A reserved list for you, maybe you wanna use it */
    mainData?: T[];

    /**
     * Deleted items manually & locally in table is the main purpose of this prop.
     * When you delete an item from table, this item has been added to this prop list automatically.
     */
    deletedItems?: T[];

    /** Added items manually & locally to table is the main purpose of this prop */
    localData?: T[];

    /** List your field names to prevent sorting */
    noSortableCols?: string[];

    /** Count of whole data based on your API request */
    totalCount?: number;

    /** Use for filter multiple columns */
    filterItems?: IFilterComplexItems;

    /** It's obvious */
    selectedItem?: any;
}
