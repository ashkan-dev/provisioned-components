/** Sort model of col of table */
export interface ISort {
    /** Col field name */
    active: string;

    /**
     * Direction of sorting data.
     * Empty means sorting will not apply.
     */
    direction: '' | 'asc' | 'desc';
}
