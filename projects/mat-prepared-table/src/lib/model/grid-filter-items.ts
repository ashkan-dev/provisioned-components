/** Selected filter items model */
export interface IGridFilterItems {
    /** Field displayed label */
    field: string;

    /** Operand for filtering */
    operation: string;

    /** Selected value for filtering */
    value: string;
}
