import { IGridFilterItems } from './grid-filter-items';
import { IProvitionedComplexFilterItems } from './provisioned-complex-filter-items';

/** Prepare a complex of filters for your table */
export interface IFilterComplexItems {
    /** The filter items you selected before */
    items?: IGridFilterItems[];

    /** Items to prepare filtering UI */
    provisionedItems: IProvitionedComplexFilterItems[];
}
