import { ISort } from './sort';

/** Model for prepare req from API */
export interface IDataGridEntity {
    /** Search text entered in search text box of table */
    searchText?: string;

    /** Page idx specified in table */
    pageIdx: number;

    /** Number of data displaying in each page specified in tbl */
    pageSize: number;

    /** Sort data model */
    sort?: ISort;
}
