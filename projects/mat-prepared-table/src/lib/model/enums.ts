/** Control type of objects you want to use in mat-prepared-table component */
export enum ControlType {
    List = 'List',
    Text = 'Text',
    Checkbox = 'Checkbox',
    Cell = 'Cell',
    Num = 'Number',
    Link = 'Link'
}
