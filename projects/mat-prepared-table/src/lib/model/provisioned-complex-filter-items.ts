import { KeyValue } from '@angular/common';
import { ControlType } from './enums';

/**
 * Prepare your table fields with appropriate components to filter you table.
 * You should prepare it in tableInput parameter.
 */
export interface IProvitionedComplexFilterItems {
    /** Field displayed label */
    field: string;

    /** The type of your field as a component to prepare in filter dialog */
    type: ControlType;

    /** Use this when your field type is List to prepare list values */
    values?: KeyValue<string, string>[];
}
