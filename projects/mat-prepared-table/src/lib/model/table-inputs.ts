import { ITableItems } from './table-items';

/** Table model for preparation UI & data */
export interface ITableInputs<T> {
    /** Data of related model */
    data?: ITableItems<T>;

    /** Disable table */
    disable?: boolean;

    /** Disable paginator */
    disablePager?: boolean;

    /** Disable search text box & button */
    disableSearch?: boolean;

    /** Specify either display the add button in toolbar of table or not */
    isAddButtonVisible?: boolean;

    /** A back button for any purpose you need */
    isBackButtonVisible?: boolean;

    /** Specify delete button visibility in toolbar for deleting row */
    isDeleteButtonVisible?: boolean;

    /** Specify edit button visibility in toolbar for editing row */
    isEditButtonVisible?: boolean;

    /** Specify visibility of displaying page number digits */
    isPageNumberVisible?: boolean;

    /** Specify either display the page size change component in paginator of table or not */
    isPageSizeChangeable?: boolean;

    /** Specify visibility of paginator of table */
    isPaginatorVisible?: boolean;

    /** Specify either display the search text box or not */
    isSearchBoxVisible?: boolean;

    /** Ability for multi select rows */
    multiSelector?: boolean;

    /** Default number of displaying data in each page of table */
    pageSize?: number;

    /** Page sizes for data in table paginator */
    pageSizes?: number[];

    /** Enable row dbl click event */
    rowDblClickEnable?: boolean;

    /** Use for sorting data locally */
    sortLocalData?: boolean;

    /** The title of your table */
    title?: string;
}
